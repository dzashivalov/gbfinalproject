# Output for Loadbalancer public IP
output "lb_fip_address" {
  value = vkcs_networking_floatingip.project_lb_fip.address
}

# Outputs for apps public IPs
output "app01_fip_address" {
  value = vkcs_networking_floatingip.app01_fip.address
}
output "app02_fip_address" {
  value = vkcs_networking_floatingip.app02_fip.address
}

# Output for NFS public IP
output "nfs_fip_address" {
  value = vkcs_networking_floatingip.nfs01_fip.address
}

# Output for NFS local IP
output "nfs_lip_address" {
  value = vkcs_compute_instance.nfs01.access_ip_v4
}

# Output for DB local IP
output "db_lip_address" {
  value = vkcs_db_instance.db01.ip[0]
}

# Output for Mon Public IP
output "mon_fip_address" {
  value = vkcs_networking_floatingip.mon_fip.address
}

# Output for Private Key
output "private_key" {
  value = vkcs_compute_keypair.keypair.private_key
}