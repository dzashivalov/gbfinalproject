# ---------------------------------------------- #
#               Auth to providers                #
# ---------------------------------------------- #

# Authenticate to VK Cloud
provider "vkcs" {
  region   = "RegionOne"
  auth_url = "https://infra.mail.ru:35357/v3/"
}
# Define VK Cloud terraform provider
terraform {
  backend "s3" {
    endpoint                    = "https://hb.vkcs.cloud"
    region                      = "ru-msk"
    bucket                      = "project-bucket"
    key                         = "terraform.tfstate"
    # Without specifying these keys, Terraform will not be able to access AWS resources and will return a 403 error.
    skip_region_validation = true
    skip_credentials_validation = true
    skip_metadata_api_check = true
    skip_requesting_account_id = true
    skip_s3_checksum = true
  }
  required_providers {
    vkcs = {
      source  = "vk-cs/vkcs"
      version = "~> 0.4.0"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Create a keypair for product deployment
resource "vkcs_compute_keypair" "keypair" {
  name = var.keypair_name
}