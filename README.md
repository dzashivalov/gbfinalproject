
## Описание  

Проект использует Terraform для управления инфраструктурой и Ansible для распространения ПО.  
Файл состояния terraform.tfstate проекта находится в S3 бакет VK Cloud.  
План резервного копирования для **Database** и **NFS** серверов настроен на создание бэкапов каждые 3 часа и хранения максимум 3 полных копий.  

### Схема проекта
![enter image description here](https://gitlab.com/dzashivalov/gbfinalproject/-/raw/main/images/scheme.png)
  
### Важно!  
После распространения ПО, ssl сертификат хранится на трех хостах:  
| Имя | Путь privkey | Путь fullchain|
| :- | :- | :- |
|**App01**|/etc/nginx/ssl/privkey.pem|/etc/nginx/ssl/fullchain.pem|
|**App02**|/etc/nginx/ssl/privkey.pem|/etc/nginx/ssl/fullchain.pem|
|**Mon**|/etc/grafana/grafana.key|/etc/grafana/grafana.crt|

Пайплайн содержит 5 стейджей:
| Имя | Описание | Триггер | Тип запуска | Зависит от |
| :- | :- | :-: | :-: | :-: |
|**plan**|Выполняет валидацию конфигурационного файла Terraform|Изменения в main ветке|**Авто**|**x**|
|**apply**|Примененяет конфигурацию|Изменения в main ветке|**Авто**|**plan**|
|**destroy**|Удаляет существующую инфраструктуру|**x**|**Ручной**|**apply**|
|**pre-deploy**|Выгружает необходимые данные (такие как ip-адреса и ключи) для следующего шага|**x**|**Ручной**|**apply**|
|**deploy**|Распространяет ПО на инфраструктуру (созданную на стейдже **apply**)|**x**|**Авто**|**pre-deploy**|
  
Ресурсы необходимые для проекта:
| Имя | Значение |
| :- | :-: |
|**Instances**|5|
|**CPU**|5|
|**RAM**|16384MB|
|**Volumes**|5|
|**Storage**|100GB|
|**IP-address Neutron**|5|
|**LBaaS Neutron**|1|  

## Применение    
  
1. [Создать аккаут и бакет в личном кабинете VK Cloud](https://cloud.vk.com/docs/ru/storage/s3/quick-start). *  
    \* Access Key и Secret key понадобятся при заполнении переменных проекта ниже.  
2. Склонировать данный репозиторий к себе на GitLab.  
3. Скачать openrc.sh из личного кабинета.  
   - Пункт номер 3 по [ссылке](https://cloud.vk.com/docs/tools-for-using-services/cli/openstack-cli).  
4. Создать Telegram бот и канал с ним:  
   - Отправить в чат с [@BotFather](https://telegram.me/BotFather) команду /newbot.  
   - Ввести название бота.  
   - Ввести юзернейм бота, который будет отображаться в адресной строке.  
   - После создания бота, [@BotFather](https://telegram.me/BotFather) вернет Вам token бота. * 
   - Создать канал.  
   - Пригласить в него созданного выше бота.  
   - Переслать сообщение из выше созданного канала боту [@my_id_bot](https://telegram.me/my_id_bot).  
   - [@my_id_bot](https://telegram.me/my_id_bot) вернет Вам id канала.  
    \* Token бота и id чата понадобятся для подключения оповещений на GitLab и при заполнении переменных проекта ниже.  
5. Зарегестрировать доменное имя. *  
    \* Тут ограничений нет, можно воспользоваться любым сервисом, например [GoDaddy.com](GoDaddy.com).  
6. Выпустить сертификат для домена. *  
    \* Тут ограничений нет, можно воспользоваться бесплатным [LetsEncrypt.org](LetsEncrypt.org) или любым другим сервисом.  
7. Скопировать файлы сертификата домена **fullchain.pem** и **privkey.pem** в репозиторий по пути: **deploy/roles/ssl/files**.  
8. Подключить оповещения GitLab.  
   - На странице Вашего проекта пройти в **Settings -> Integration -> Telegram -> Configure**.  
   - **Channel identifier** - id канала из пункта 4.  
   - **Token** - token бота из пункта 4.  
   - Снять чекбокс с Notify only broken pipelines
   - Выбрать следующие триггеры в **Trigger**.  
        - A push is made to the repository
        - A merge request is created, merged, closed, or reopened
        - A pipeline status changes
   - В **Branches for which notifications are to be sent** выбрать **Default branch**.  
   - Нажать **Save changes**.  
9. Создать и заполнить переменные в настройках проекта в соответствии с таблицей ниже: *  
   - На странице Вашего проекта пройти в **Settings -> CI/CD -> Variables -> Expand**.
   - Для создания переменной нажмите **Add variable**.

| Имя | Описание | Видимость | Значение при тестировании в проекте |
| :- | - | - | :-: |
|AWS\_ACCESS\_KEY\_ID|VK Cloud S3 Access Key|Visible||
|AWS\_SECRET\_ACCESS\_KEY|VK Cloud S3 Secret key|Masked||
|CI\_OS\_PROJECT\_ID|VK Cloud из openrc.sh файла|Visible||
|CI\_OS\_USERNAME|Логин VK Cloud|Visible|||
|CI\_OS\_PASSWORD|Пароль VK Cloud|Masked|||
|TF\_VAR\_wp\_database|Имя базы данных WordPress|Visible||
|TF\_VAR\_wp\_password|Пароль базы данных WordPress|Masked||
|TF\_VAR\_wp\_username|Имя пользователя базы данных WordPress|Visible||
|bot\_chat|Telegram Chat ID|Masked||
|bot\_token|Telegram Bot Token|Masked||
|alertmanager\_version|AlertManager Версия|Visible|0.25.0|
|grafana\_version|Grafana Версия|Visible|9.4.3|
|nginx\_exporter\_version|Nginx Exporter Версия|Visible|0.11.0|
|node\_exporter\_version|Node Exporter Версия|Visible|1.5.0|
|prometheus\_version|Prometheus Версия|Visible|2.43.0-rc.0|
|domain\_name|Your Domain Name|Visible|

10. Если установка wordpress не должна производиться в автоматическом режиме, из плейбука deploy/app_server.yml нужно удалить роль **wordpress**.
11. Запустить стейдж **plan**, начнется применение конфигурации Terraform.
12. После успешного завершения стейджа **plan**, в файле **.gitlab-ci.yml** в стейдже **pre-deploy** убрать строку **when: manual** и закоммитить - Ansible прольет плейбуки из каталога deploy на созданную инфраструктуру.
12. Теперь после измений в master ветке всегда будет запускаться стейдж **pre-deploy** и **deploy**.
13. После успешного выполнения стейджей plan, apply, pre-deploy и deploy, приложение будет доступно по адресу указанному в переменной **domain_name** - **[https://domain_name]()**, мониторинг приложения будет доступен по адресу - **[https://mon.domain_name]()**.  
14. Файл ключа для подключения к виртуальным машином можно скачать в артифактах стейджа **pre-deploy** \*  
    \*  Build -> Jobs -> Иконка со стрелкой вниз справа от выполненного стейджа **pre-deploy**