# Install AlertManager
- name: Create the alertmanager group
  group:
    name: alertmanager
    state: present
    system: yes
  become: yes

- name: Create the alertmanager user
  user:
    name: alertmanager
    groups: alertmanager
    append: yes
    shell: /usr/sbin/nologin
    system: yes
    create_home: no
  become: yes

- name: Create necessary directories
  file:
    path: "{{ item }}"
    state: directory
    owner: alertmanager
    group: alertmanager
    mode: 0775
  loop:
    - /etc/alertmanager
    - /etc/alertmanager/template
    - /var/lib/alertmanager/
    - /var/lib/alertmanager/data
  become: yes

- name: Download and Unpack Alertmanager
  unarchive:
    src: "https://github.com/prometheus/alertmanager/releases/download/v{{ alertmanager_version }}/alertmanager-{{ alertmanager_version }}.linux-amd64.tar.gz"
    dest: "/tmp"
    remote_src: yes
  become: no

- name: Propagate Alertmanager binaries
  copy:
    src: "{{ item.src }}" 
    dest: "{{ item.dest }}"
    remote_src: yes
    owner: alertmanager
    group: alertmanager
    mode: 0755
  loop:
    - src: /tmp/alertmanager-{{ alertmanager_version }}.linux-amd64/alertmanager
      dest: /usr/local/bin/
    - src: /tmp/alertmanager-{{ alertmanager_version }}.linux-amd64/amtool
      dest: /usr/local/bin/
  register: alertmanager_copied
  become: yes

- name: Configure Alertmanager
  template:
     src: alertmanager.yml.j2 
     dest: /etc/alertmanager/alertmanager.yml
  become: yes

- name: Create systemd daemon
  template:
     src: alertmanager.service.j2
     dest: /etc/systemd/system/alertmanager.service
  become: yes

- name: Enable Alertmanager service
  systemd:
     name: alertmanager
     daemon_reload: yes
     enabled: yes
     state: started
  become: yes

- name: Cleanup /tmp
  file:
    path: "{{ item }}"
    state: absent
  loop:
    - /tmp/alertmanager-{{ alertmanager_version }}.linux-amd64
    - /tmp/alertmanager-{{ alertmanager_version }}.linux-amd64.tar.gz
  when: alertmanager_copied is succeeded
  become: yes